# Coffee delivery comparison

## Previous products delivery session:

{"Id":1,"Name":"Coffee Columbia","Features":[{"Name":"Flavor","Description":"Mild"},{"Name":"Acidity","Description":"5"}]}<br />
{"Id":2,"Name":"Coffee Guatemala","Features":[{"Name":"Flavor","Description":"Strong"},{"Name":"Acidity","Description":"7"}]}<br />
{"Id":3,"Name":"Coffee Brazil","Features":[{"Name":"Flavor","Description":"Soft"},{"Name":"Acidity","Description":"4"}]}<br />

## Current delivery session:

{"Id":1,"Name":"Coffee Columbia","Features":[{"Name":"Flavor","Description":"Mild"},{"Name":"Acidity","Description":"5"}]}<br />
{**"Id":2,"Name":"Coffee Guatemala",**"Features":[{"Name":"Flavor","Description":"Strong"},{"Name":"Acidity","Description":**"7" -> "9"**}]}<br />
~~{"Id":3,"Name":"Coffee Brazil","Features":[{"Name":"Flavor","Description":"Soft"},{"Name":"Acidity","Description":"4"}]}~~<br />
{**"Id":4,"Name":"Coffee Honduras"**,**"Features":[{"Name":"Flavor","Description":"Soft"},{"Name":"Acidity","Description":"4"}**]}<br />
