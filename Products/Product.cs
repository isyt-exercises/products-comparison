namespace Products;

public class Product {
    public int Id {get;set;}
    public string Name {get;set;}

    public IList<ProductFeature> Features {get;set;}
}