namespace Products;

public class ProductFeature
{
    public string Name { get; set; }
    public string Description { get; set; }

    public ProductFeature(string name, string description)
    {
        Name = name;
        Description = description;
    }
}