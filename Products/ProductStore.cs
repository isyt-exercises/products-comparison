namespace Products;

public class ProductStore {
    public IList<Product> GetByDeliverySession(int sessionId){
        switch (sessionId)
        {
            case 1: return BuildBasicList();
            case 2:
            {
                var sessionTwo = BuildBasicList();

                sessionTwo[1].Features[1].Description = "9";
                sessionTwo[2].Id = 4;
                sessionTwo[2].Name = "Coffee Honduras";

                return sessionTwo;
            }
            default: throw new NotImplementedException();
        }
    }

    private IList<Product> BuildBasicList()
    {
        return new List<Product>() {
            new Product() { 
                Id = 1, 
                Name = "Coffee Columbia", 
                Features = new List<ProductFeature>() {new ProductFeature("Flavor","Mild"), new ProductFeature("Acidity", "5")}
            },
            new Product()
            {
                Id = 2, 
                Name = "Coffee Guatemala",
                Features = new List<ProductFeature>() {new ProductFeature("Flavor","Strong"), new ProductFeature("Acidity", "7")}
            },
            new Product()
            {
                Id = 3, 
                Name = "Coffee Brazil",
                Features = new List<ProductFeature>() {new ProductFeature("Flavor","Soft"), new ProductFeature("Acidity", "4")}
            }
        };
    }
}