﻿using System.Text.Json;
using Products;

var productStore = new ProductStore();

ShowProductList(productStore.GetByDeliverySession(1).ToList());
ShowProductList(productStore.GetByDeliverySession(2).ToList());


void ShowProductList(List<Product> products)
{
    Console.WriteLine("Products:");
    products.ForEach(product => Console.WriteLine(JsonSerializer.Serialize(product)));
}
